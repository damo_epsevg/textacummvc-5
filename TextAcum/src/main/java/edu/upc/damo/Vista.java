package edu.upc.damo;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by Josep M on 09/10/2014.
 * <p/>
 * Responsable de la presentació
 * Observa el model i s'actualitza quanaquest l'avisa
 */
public class Vista implements MainActivity.OnCanviModelListener{
    private TextView res;

 public Vista(Activity a) {
        res = (TextView) a.findViewById(R.id.resultat);
        ((MainActivity) a).setOnCanviModelListener(new MainActivity.OnCanviModelListener() {
            @Override
            public void onCanviModel(ModelObservable model) {
                refesPresentacio(model);
            }
        });

    }


    /*  Codi per eliminar l'efecte lateral


    public Vista(Activity a){
        res = (TextView) a.findViewById(R.id.resultat);
    }

    public void enregistraVista(MainActivity a) {
        a.setOnCanviModelListener(new MainActivity.OnCanviModelListener() {
            @Override
            public void onCanviModel(ModelObservable model) {
                refesPresentacio(model);
            }
        });
    }


  */


    public void refesPresentacio(ModelObservable model) {
        res.setText("");
        int i = 0;
        for (CharSequence s : model) {
            if (i != 0)
                res.append("\n");
            res.append(String.valueOf(i + 1));
            res.append(" ");
            res.append(s);
            i++;
        }


    }

    @Override
    public void onCanviModel(ModelObservable model) {
        refesPresentacio(model);
    }



}
